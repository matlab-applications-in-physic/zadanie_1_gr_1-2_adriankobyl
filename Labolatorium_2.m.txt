% THIS IS NOT A CODE OF NEITHER MATLAB OR SCILAB SCRIPT

polecenie 1
 h=6.62607015e-34 % Planck constant declaration
h/(2*pi)= 1.0546e-034 % score

polecenie 2
 e=exp(1)= 2.7183 % constant declaration - the natural logarithm exponent

polecenie 3
 hex2dec('00123d3')=74707 % conversion from hexadecimal to decimal
  74707/(2.455e23)=3.0431e-019 % score
  
  polecenie 4
   sqrt((e-pi))= 0 + 0.6506i % complex result
   
   polecenie 7
    liczba=hex2dec('aabb')=43707 % conversion from hexadecimal to decimal
    Rz=6371 % constant declaration - radius of the earth in km
     A=sqrt(7)/2=1.3229 % declaration of constant A for simplification of calculations
     B=log(Rz/10^5)=-2.7534 % declaration of constant B for simplification of calculations
     atan((e^(A-B))/43707)= 0.0013 % score in radians
     
     
   
   